<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>String PHP</title>
</head>
<body>
    <h1>Berlatih String PHP</h1>
    <?php   
        echo "<h3> Soal No 1</h3>";
        /* 
            SOAL NO 1
            Tunjukan dengan menggunakan echo berapa panjang dari string yang diberikan berikut! Tunjukkan juga jumlah kata di dalam kalimat tersebut! 

            Contoh: 
            $string = "PHP is never old";
            Output:
            Panjang string: 16, 
            Jumlah kata: 4 
        */

        $first_sentence = "Hello PHP!" ;
        echo "<b>Hello PHP!</b>" . "<br>";
        // Print Panjang String
        echo "Panjang string adalah : " . strlen($first_sentence) . "<br>";
        // Print Jumlah Kata
        echo "Jumlah Kata : " . str_word_count($first_sentence) . "<br> <br>";

        // Soal ke 1 COntoh kedua
        $second_sentence = "I'm ready for the challenges";
        echo "<b>I'm ready for the challenges</b>" . "<br>";
        // Print Panjang String
        echo "Panjang string adalah : " . strlen($second_sentence) . "<br>";
        // Print Jumlah Kata
        echo "Jumlah Kata : " . str_word_count($second_sentence) . "<br><br>";


        echo "<h3> Soal No 2</h3>";
        
        $string2 = "I love PHP";
        
        echo "<label>String: </label> \"$string2\" <br>";
        echo "Kata pertama: " . substr($string2, 0, 1) . "<br>" ; 
        // Lanjutkan di bawah ini
        echo "Kata Kedua: " . substr($string2, 2, 5);
        echo "<br> Kata Ketiga: " . substr($string2, 6, 8) . "<br>" ;

        echo "<h3> Soal No 3 </h3>";
        /*
            SOAL NO 3
            Mengubah karakter atau kata yang ada di dalam sebuah string.
        */
        $string3 = "PHP is old but sexy!";
        echo "Kalimat utama". "<br>";
        echo "<b>String: \"$string3\" </b>" . "<br><br>"; 
        // OUTPUT : "PHP is old but awesome"
        echo "Kalimat edit". "<br>";
        echo str_replace("sexy", "awesome",$string3);
    ?>
</body>
</html>